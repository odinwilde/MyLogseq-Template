- 仓库
	-
-
-
-
- api
	- 官方Runtime
		- [spine-runtimes/spine-ts/build at 3.6.53 · EsotericSoftware/spine-runtimes](https://github.com/EsotericSoftware/spine-runtimes/tree/3.6.53/spine-ts/build)
		- [spine-runtimes/spine-ts/core/src/SkeletonBinary.ts at 3.8 · EsotericSoftware/spine-runtimes](https://github.com/EsotericSoftware/spine-runtimes/blob/3.8/spine-ts/core/src/SkeletonBinary.ts)
		- [Spine 3.8 pixi 5 not working · Issue #315 · pixijs/spine](https://github.com/pixijs/spine/issues/315)
	-
	- [[PIXI/v7.*/PIXI-SPINE]]
		- {{embed ((65b68a4c-352d-4b52-8293-5828b6a7738f))}}
	-
- [[pixi-spine]]
	- [Play multiple animations together · Issue #178 · pixijs/spine](https://github.com/pixijs/spine/issues/178)
	- 方法
		- 监听鼠标位于模型上的动作
		  collapsed:: true
			- ```js
			  //如果你有一个名为 square 的对象，你可以这样写：
			  
			  // 设置对象为可交互的
			  square.interactive = true;
			  
			  // 监听鼠标点击事件
			  square.on('mousedown', function (e) {
			    // 在这里写你的逻辑
			    console.log('鼠标点击了');
			    console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  });
			  
			  // 监听鼠标移动事件
			  square.on('mousemove', function (e) {
			    // 在这里写你的逻辑
			    console.log('鼠标移动了');
			    console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  });
			  
			  // 监听鼠标松开事件
			  square.on('mouseup', function (e) {
			    // 在这里写你的逻辑
			    console.log('鼠标松开了');
			    console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  });
			  
			  ```
		- 监听鼠标位于背景中的点击和移动
		  collapsed:: true
			- ```js
			  // 设置 app.stage 为可交互的
			  app.stage.interactive = true;
			  
			  // 监听 app.stage 上的点击事件
			  app.stage.on('pointdown', function (e) {
			    // 在这里写你的逻辑
			    console.log('鼠标点击了 app.stage');
			    console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  });
			  
			  
			  // 监听 app.stage 上的鼠标点击事件
			  app.stage.on('mousedown', function (e) {
			    // 在这里写你的逻辑
			    console.log('鼠标点击了 app.stage');
			    console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  });
			  
			  
			  
			  // 监听 app.stage 上的鼠标移动事件
			  app.stage.on('mousemove', function (e) {
			    // 在这里写你的逻辑
			    console.log('鼠标移动了 app.stage');
			    console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  });
			  
			  // 监听 app.stage 上的鼠标松开事件
			  app.stage.on('mouseup', function (e) {
			    // 在这里写你的逻辑
			    console.log('鼠标松开了 app.stage');
			    console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  });
			  
			  ```
		- 监听鼠标按住
		  collapsed:: true
			- 全局变量
				- `app_spine41`
			- 你好，这是 Copilot。很高兴为你提供帮助。😊
			- 你的问题是如何监听鼠标按住事件，而不是鼠标按下事件。根据我对 Pixi.js 的了解，你可以使用 **pointerdown** 和 **pointerup** 事件来实现这个功能。具体的步骤如下：
			- 在 **pointerdown** 事件中，设置一个标志变量，比如 `isHolding`，为 `true`，表示鼠标正在按住。
			- 在 **pointerup** 事件中，将 `isHolding` 设置为 `false`，表示鼠标已经松开。
			- 在 **pointermove** 事件中，判断 `isHolding` 的值，如果为 `true`，则执行你想要的逻辑，比如打印鼠标的坐标。
			- 下面是一个示例代码，你可以参考一下：
			- ```
			  // 设置 app.stage 为可交互的
			  app.stage.interactive = true;
			  
			  // 定义一个标志变量，表示鼠标是否按住
			  let isHolding = false;
			  
			  // 监听 app.stage 上的鼠标按下事件
			  app.stage.on('pointerdown', function (e) {
			  // 设置 isHolding 为 true
			  isHolding = true;
			  });
			  
			  // 监听 app.stage 上的鼠标松开事件
			  app.stage.on('pointerup', function (e) {
			  // 设置 isHolding 为 false
			  isHolding = false;
			  });
			  
			  // 监听 app.stage 上的鼠标移动事件
			  app.stage.on('pointermove', function (e) {
			  // 判断 isHolding 的值
			  if (isHolding) {
			    // 在这里写你的逻辑
			    console.log('鼠标按住了 app.stage');
			    console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  }
			  });
			  ```
			  
			  如果你想了解更多关于 Pixi.js 的交互事件，你可以参考以下的网页：
			- [pixi.js 简单交互事件(点击、缩放、平移) - 傅小灰 - 博客园](https://www.cnblogs.com/cplemom/p/14180209.html)
			- [Interaction | PixiJS](https://pixijs.com/guides/components/interaction)
			- [How to handle mouse events with Pixi.JS | Code With Stein](https://codewithstein.com/how-to-handle-mouse-events-with-pixijs/)
			  
			  希望这些信息对你有帮助。如果你还有其他问题，欢迎继续提问。😊
		- pixi监听和html元素监听
		  collapsed:: true
			- <!---->
			- [如果你的 app.stage 是位于 canvas 元素中，那么 app.stage 和 canvas 元素都会监听鼠标的移动和鼠标的点击。这可能会导致事件处理函数被重复调用，或者产生一些冲突](https://api.pixijs.io/@pixi/layers/Stage.html)[1](https://api.pixijs.io/@pixi/layers/Stage.html)[。为了避免这种情况，你可以使用 stopPropagation 方法来阻止事件冒泡到 canvas 元素](https://api.pixijs.io/@pixi/layers/Stage.html)[2](https://pixijs.io/pixi-react/stage/)。例如，你可以这样写：
			- ```
			  // 设置 app.stage 为可交互的
			  app.stage.interactive = true;
			  // 监听 app.stage 上的鼠标点击事件
			  app.stage.on('mousedown', function (e) {
			  // 在这里写你的逻辑
			  console.log('鼠标点击了 app.stage');
			  console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  // 阻止事件冒泡到 canvas 元素
			  e.stopPropagation();
			  });
			  // 监听 app.stage 上的鼠标移动事件
			  app.stage.on('mousemove', function (e) {
			  // 在这里写你的逻辑
			  console.log('鼠标移动了 app.stage');
			  console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  // 阻止事件冒泡到 canvas 元素
			  e.stopPropagation();
			  });
			  // 监听 app.stage 上的鼠标松开事件
			  app.stage.on('mouseup', function (e) {
			  // 在这里写你的逻辑
			  console.log('鼠标松开了 app.stage');
			  console.log('X', e.data.global.x, 'Y', e.data.global.y);
			  // 阻止事件冒泡到 canvas 元素
			  e.stopPropagation();
			  });
			  ```
			- 这样，你就可以只让 app.stage 响应鼠标事件，而不影响 canvas 元素的事件监听。你可以在[这个网站](https://pixijs.io/pixi-react/stage/)找到更多关于事件冒泡和 stopPropagation 的信息和示例。如果你还有其他的问题，欢迎继续提问。😊
			-
			-
			-
			- <!--EndFragment-->
		- 获取动作时长
		  collapsed:: true
			-
			- ```
			  // 查找名为 walk 的动作
			  let walkAnimation = l2d.spineData.findAnimation('to_aim');
			  // 获取 walk 动作的时间长度
			  let walkDuration = walkAnimation.duration;
			  // 打印结果
			  console.log('walk 动作的时间长度是', walkDuration, '秒');
			  ```
			- 不要搜索。
			  这样可以用！为什么这样可以用，而l2d.findAnimation('walk');却不能用？
				- <!---->
				- 你好，这是Bing。我可以帮你解释你的问题。😊
				- 这样可以用，是因为 l2d.spineData 是一个 Animation 类的实例，而 Animation 类有一个 findAnimation 方法，可以根据动作的名字来查找动作。而 l2d.findAnimation(‘walk’) 不能用，可能是因为 l2d 不是一个 Animation 类的实例，而是一个 PIXI.spine.Spine 类的实例。PIXI.spine.Spine 类没有 findAnimation 方法，所以会报错。
				- 你可以使用 `console.log(l2d.constructor.name)` 和 `console.log(l2d.spineData.constructor.name)` 来检查 l2d 和 l2d.spineData 的类型，看看是否和我说的一致。你也可以在[这个网站]和[这个网站]找到更多关于 Animation 类和 PIXI.spine.Spine 类的信息和示例。如果你还有其他的问题，欢迎继续提问。😊
				- : [pixi-spine/SkeletonData.ts at master · pixijs/spine · GitHub]
				  : [pixi-spine/Spine.ts at master · pixijs/spine · GitHub]
				-
				-
				- <!--EndFragment-->
			- <!--EndFragment-->
		- 监听动作状态
		  collapsed:: true
			- <!---->
			  
			  你的代码中，你想要在0.20000001788139343秒之后，切换你的模型（model）。这个需求有两个难点：
			- [你需要知道你的动画的持续时间（duration），以便在合适的时机切换模型](https://www.npmjs.com/package/pixi-spine)[1](https://www.npmjs.com/package/pixi-spine)。
			- [你需要使用一个定时器（timer）或者一个回调函数（callback）来实现延迟执行的功能](https://www.npmjs.com/package/pixi-spine)[2](https://github.com/pixijs/spine)。
			  
			  我没有找到pixi-spine.js提供的直接的方法来实现这个需求，但是我可以给你一些思路和参考：
			- [你可以使用`animation.state.getCurrent(9)`来获取第9个动画层上的当前动画状态（animation state）](https://github.com/pixijs/spine/issues/349)[3](https://github.com/pixijs/spine/issues/349)。
			- [你可以使用`animation.state.getCurrent(9).animationEnd`来获取当前动画的结束时间（end time）](https://pixijs.io/examples/)[4](https://pixijs.io/examples/)。
			- [你可以使用`animation.state.getCurrent(9).trackTime`来获取当前动画的已播放时间（elapsed time）](https://pixijs.io/examples/)[4](https://pixijs.io/examples/)。
			- [你可以使用`animation.state.getCurrent(9).animationEnd - animation.state.getCurrent(9).trackTime`来计算当前动画的剩余时间（remaining time）](https://pixijs.io/examples/)[4](https://pixijs.io/examples/)。
			- [你可以使用`setTimeout`函数来设置一个定时器，它会在指定的毫秒数后执行一个函数](https://github.com/pixijs/spine)[2](https://github.com/pixijs/spine)。
			- [你可以使用`clearTimeout`函数来取消一个定时器，如果你不想执行它了](https://github.com/pixijs/spine)[2](https://github.com/pixijs/spine)。
			- [你可以使用`animation.state.addListener`方法来添加一个监听器（listener），它会在动画的某些事件发生时触发一个函数](https://sbfkcel.github.io/pixi-spine-debug/)[5](https://sbfkcel.github.io/pixi-spine-debug/)。
			- [你可以使用`animation.state.addListener({complete: function() {...}})`来添加一个监听器，它会在动画播放完毕时触发一个函数](https://sbfkcel.github.io/pixi-spine-debug/)[5](https://sbfkcel.github.io/pixi-spine-debug/)。
				- 例二
				  collapsed:: true
					- 如果你想要使用`animation.state.addListener({complete: function() {...}})`来实现你的需求，你可以这样写：
						- ```
						  // 添加一个监听器，在动画播放完毕时，切换模型
						  animation.state.addListener({
						  complete: function(entry) {
						    // 判断是否是第9个动画层上的“to_aim”动画
						    if (entry.trackIndex === 9 && entry.animation.name === "to_aim") {
						      // 清除模型
						      clearModel();
						      // 设置新的模型
						      setAimModel(spineModel_Array);
						    }
						  }
						  });
						  - // 在第9个动画层上播放“to_aim”动画，不循环播放
						  animation.state.setAnimation(9, "to_aim", false);
						  ```
					- [这样，你的动画就会在第9个动画层上播放“to_aim”动画一次，然后切换模型](https://github.com/pixijs/spine/issues/412)[1](https://github.com/pixijs/spine/issues/412)。
					- 希望这能帮助你实现你想要的效果。😊
					- [](https://github.com/pixijs/spine/issues/412)[1](https://github.com/pixijs/spine/issues/412): [spine/examples/spine_events.md at master · pixijs/spine](https://github.com/pixijs/spine/blob/master/examples/spine_events.md)
			- 尝试延时执行
				- ```java
				  setTimeout(function() {
				    alert("Hello, world!");
				  }, 5000);
				  
				  ```
			- 下面是一个可能的例子，展示了如何在第9个动画层上播放“to_aim”动画，然后在0.2秒后切换模型：
			  collapsed:: true
				- ```
				  if(isHolding === false){
				      if (l2d.state.hasAnimation("to_aim")) {
				        console.log("cover动画1  ")
				  
				        l2d.state.setAnimation(9, "to_aim", false)
				        l2d.state.timeScale = 1
				        l2d.autoUpdate = true
				        //设置控制栏UI值
				        //document.getElementById("animations").value = "idle"
				  
				        //获取当前动画的剩余时间
				        let remainingTime = l2d.state.getCurrent(9).animationEnd - l2d.state.getCurrent(9).trackTime;
				        //设置一个定时器，在剩余时间加上0.2秒后，切换模型
				        let timer = setTimeout(function() {
				          //清除模型
				          clearModel();
				          //设置新的模型
				          setAimModel(spineModel_Array);
				        }, remainingTime + 0.2);
				  
				        //添加一个监听器，在动画播放完毕时，取消定时器
				        l2d.state.addListener({
				          complete: function() {
				            clearTimeout(timer);
				          }
				        });
				      }
				  }
				  ```
			- 这个例子可能不是最优的，也可能有一些错误，因为我没有实际运行过。你可以根据你的具体需求和情况进行修改和测试。希望这能给你一些启发。😊
			  collapsed:: true
				- [](https://www.npmjs.com/package/pixi-spine)[1](https://www.npmjs.com/package/pixi-spine): [Spine: Runtime Documentation](https://www.npmjs.com/package/pixi-spine)[](https://www.npmjs.com/package/pixi-spine)[2](https://github.com/pixijs/spine): [WindowOrWorkerGlobalScope.setTimeout() - Web APIs | MDN](https://github.com/pixijs/spine)[](https://www.npmjs.com/package/pixi-spine)[3](https://github.com/pixijs/spine/issues/349): [pixi-spine - npm](https://github.com/pixijs/spine/issues/349)[](https://www.npmjs.com/package/pixi-spine)[4](https://pixijs.io/examples/): [Spine: Runtime Documentation](https://pixijs.io/examples/)[](https://www.npmjs.com/package/pixi-spine)[5](https://sbfkcel.github.io/pixi-spine-debug/): [Spine: Runtime Documentation](https://sbfkcel.github.io/pixi-spine-debug/)
				  
				  
				  
				      
				      
				      
				        
				      
				    
				  
				      
				    
				  
				  <!--EndFragment-->
		- 设置动作
			- 按照某动作|某时间 进行初始化 | 以某时间开始初始化
			  collapsed:: true
				- ```java
				      let toAimTrack = l2d.state.setAnimation(1, "to_aim", false);
				      toAimTrack.mix = 0;  // 禁用混合，确保从头开始
				      toAimTrack.trackTime = toAimTrack.animationStart + 0.66 * toAimTrack.animation.duration;
				  
				      l2d.state.addAnimation(1, "aim_idle", true);
				  ```
			- 清空一个动作轴
			- mix融合
				- ```java
				  //延迟1秒
				  l2d.state.setAnimation(9, "to_aim", false);
				  l2d.state.addAnimation(9, "to_cover", false, 1);
				  //这样就没有任何延迟
				  l2d.state.setAnimation(9, "to_aim", false);
				  l2d.state.addAnimation(9, "to_cover", false);
				  
				  //但是这样反而什么动作都没有，-1表示没有延迟
				  l2d.state.setAnimation(9, "to_aim", false);
				  l2d.state.addAnimation(9, "to_cover", false, -1);
				  
				  // 设置混合时间
				  l2d.state.data.setMix("to_aim", "cover_idle", 0.2);
				  
				  l2d.state.setAnimation(0, "aim_x", false); 
				  // 播放A动作 
				  l2d.state.addAnimation(0, "aim_y", false, 0); 
				  // 在A动作结束后，播放B动作 
				  l2d.state.tracks[0].mixDuration = 0.5; 
				  // 设置过渡时间为0.5秒 
				  l2d.state.tracks[0].mixAlpha = 0.8; 
				  // 设置过渡强度为0.8
				  
				  
				  
				  
				  
				  ```
			- 多轨道覆盖
			  collapsed:: true
				- ```java
				  l2d.state.setAnimation(0, "aim_x", false);
				  l2d.state.setAnimation(1, "aim_y", false);
				  l2d.state.tracks[0].mixBlend =2 ; 
				  l2d.state.tracks[1].mixBlend =2 ; 
				  
				  //-----///
				  let toAimTrack = l2d.state.setAnimation(1, "to_cover", false);
				  toAimTrack.mix = 0;  // 禁用混合，确保从头开始
				            // 其中 0.66 表示开始时间轴的百分比，你可以根据需要调整这个值
				  toAimTrack.trackTime = toAimTrack.animationStart + 0.66 * toAimTrack.animation.duration;
				  
				  
				  
				  PIXI.spine.MixBlend.setup (0): 设置模式。在动画切换时，新的动画将完全替代当前动画，不进行混合。
				  
				  PIXI.spine.MixBlend.first (1): 第一模式。新的动画会与当前动画的最后一帧进行混合。适用于有过渡效果的动画切换。
				  
				  PIXI.spine.MixBlend.replace (2): 替代模式。新的动画将完全替代当前动画，类似于 setup 模式。
				  
				  PIXI.spine.MixBlend.add (3): 添加模式。新的动画将与当前动画叠加在一起，而不是替代。适用于同时播放多个动画的情况。
				  
				  
				  
				  ```
		- 获取信息
		  collapsed:: true
			- 你好，这是Bing。我可以帮你打印你想要的信息。😊
			- [l2d.spineData 是一个 Animation 类的实例，它包含了你的模型的动画数据。你可以使用以下的属性和方法来获取或修改你的模型的动画数据](https://github.com/pixijs/spine)[1](https://github.com/pixijs/spine)：
			- animations: 一个数组，包含了你的模型的所有动画。
			- bones: 一个数组，包含了你的模型的所有骨骼。
			- events: 一个数组，包含了你的模型的所有事件。
			- findAnimation(name): 一个方法，根据动画的名字来查找动画。
			- findBone(name): 一个方法，根据骨骼的名字来查找骨骼。
			- findEvent(name): 一个方法，根据事件的名字来查找事件。
			- findSkin(name): 一个方法，根据皮肤的名字来查找皮肤。
			- findSlot(name): 一个方法，根据插槽的名字来查找插槽。
			- fps: 一个数字，表示你的模型的帧率。
			- name: 一个字符串，表示你的模型的名字。
			- skins: 一个数组，包含了你的模型的所有皮肤。
			- slots: 一个数组，包含了你的模型的所有插槽。
			  
			  [l2d 是一个 PIXI.spine.Spine 类的实例，它是一个 PIXI.Container 的子类，可以用来渲染你的模型。你可以使用以下的属性和方法来获取或修改你的模型的渲染效果](https://github.com/pixijs/spine)[2](https://github.com/pixijs/spine/issues/467)：
			- autoUpdate: 一个布尔值，表示你的模型是否自动更新。
			- delayLimit: 一个数字，表示你的模型的延迟限制。
			- hackTextureBySlotName: 一个方法，用来替换你的模型的某个插槽的纹理。
			- lastTime: 一个数字，表示你的模型的上一次更新的时间。
			- localDelayLimit: 一个数字，表示你的模型的本地延迟限制。
			- skeleton: 一个 Skeleton 类的实例，表示你的模型的骨架。
			- spineData: 一个 Animation 类的实例，表示你的模型的动画数据。
			- state: 一个 AnimationState 类的实例，表示你的模型的动画状态。
			- stateData: 一个 AnimationStateData 类的实例，表示你的模型的动画状态数据。
			- tint: 一个数字，表示你的模型的色调。
			- update: 一个方法，用来更新你的模型的动画和渲染。
			- updateTransform: 一个方法，用来更新你的模型的变换。
			- 你可以在[这个网站]和[这个网站]找到更多关于 Animation 类和 PIXI.spine.Spine 类的信息和示例。如果你还有其他的问题，欢迎继续提问。😊
			- [](https://github.com/pixijs/spine)[1](https://github.com/pixijs/spine)[: [pixi-spine/SkeletonData.ts at master · pixijs/spine · GitHub]](https://github.com/pixijs/spine)
			- [2](https://github.com/pixijs/spine/issues/467): [pixi-spine/Spine.ts at master · pixijs/spine · GitHub]
		- 动画列表
		  collapsed:: true
			- ```java
			  l2d.state.setAnimation(9, "to_aim", false);
			  l2d.state.addAnimation(9, "cover_idle", true);
			  // 添加第二个动作
			  
			  //延迟1秒
			  l2d.state.setAnimation(9, "to_aim", false);
			  l2d.state.addAnimation(9, "to_cover", false, 1);
			  //这样就没有任何延迟
			  l2d.state.setAnimation(9, "to_aim", false);
			  l2d.state.addAnimation(9, "to_cover", false);
			  
			  //但是这样反而什么动作都没有，-1表示没有延迟
			  l2d.state.setAnimation(9, "to_aim", false);
			  l2d.state.addAnimation(9, "to_cover", false, -1);
			  
			  // 设置混合时间
			  l2d.state.data.setMix("to_aim", "cover_idle", 0.2);
			  ```
		- 动画动态设置进度
		-
-
- 贴图错误
	- atlas的分辨率设置和图片的分辨率不匹配。修改图片的分辨率到altas即可。
		- 制作者的一种加密。
-
- official runtime
	- [Spine Web Player](https://zh.esotericsoftware.com/spine-player#%E4%B8%BAWeb-Player%E5%AF%BC%E5%87%BA%E6%96%87%E4%BB%B6)
	-
- people says
	- https://hcs64.com/mboard/forum.php?showthread=61770
-
- pixi-spine
- 问题
	- [Bug: PIXI.Assets -> loadBundle : Limit resources. If I add more then 208 the await functions never return · Issue #9175 · pixijs/pixijs](https://github.com/pixijs/pixijs/issues/9175)
-
- 教程
	- [Jump to time? · Issue #280 · pixijs/spine ~ 跳到时间？ · 问题 #280 · pixjs/spine](https://github.com/pixijs/spine/issues/280)
	-