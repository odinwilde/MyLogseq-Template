-
- 地址栏复制后，在logseq粘贴没有标题。而edge是有的 #TODO
-
- [[Chrome]]插件
  id:: 645a4646-2d11-4a45-b90d-b2f6f08df5e5
	- 导出插件
	  collapsed:: true
		- 用everything搜插件-关于内的包名，如 `goutbwnjrpsjtgnioudiyuebtrrynsr`
	- [划词翻译 - Chrome 应用商店 (google.com)](https://chrome.google.com/webstore/detail/%E5%88%92%E8%AF%8D%E7%BF%BB%E8%AF%91/ikhdkkncnoglghljlkmcimlnlhkeamad)
	  collapsed:: true
	- 划词翻译
	  collapsed:: true
		- 介绍
			- 网页全文翻译需要VIP，用着谷歌和DeepL的免费api来盈利，我是不会付费的。（确实付过费。。）
		- v9.2.2 / [[DeepL]] 全网页翻译已失效
			- 217.js
				- ```java
				  pageTranslateLimit
				  videoLimit
				  ```
		- v9.8.0 / [[DeepL]] 可以
			- 63.js
				- ```java
				  const ut=114514,pt=new ke.P("pageTranslateLimit",ut)
				  ```
		- v10.1.0
			- 214.js
				- ```java
				  const mt=114514,gt=new ke.P("pageTranslateLimit",mt)
				  ```
	- [沉浸式翻译 - Chrome 应用商店 (google.com)](https://chrome.google.com/webstore/detail/immersive-translate/bpoadfkcbjbfhfodiogcnhhhpibjhbnh/related?hl=zh-CN)
	  collapsed:: true
	- 沉浸式翻译
	  collapsed:: true
		- 介绍
			- 这个也挺好用，翻译完的内容可以被[[简悦]]直接捕获，但不能切换全中文或全英文显示，只能双语同显示。开源，免费。
			- 这个插件和上面那个很像，~~我曾经记得可能在沉浸翻译里打开过划词翻译的介绍页面，感觉他俩多少有点关系？~~
	- [Link Map - Chrome 应用商店 (google.com)](https://chrome.google.com/webstore/detail/link-map/jappgmhllahigjolfpgbjdfhciabdnde)
	- linkMap
	  collapsed:: true
		- 1.1.4
			- TAB GROUP
			- TAB GROUP
			  collapsed:: true
				- ```java
				  
				  import.js
				  isProUser:!1
				  改成isProUser:!0
				    
				    
				  tree.js 同上
				  好像一直是0
				  
				  
				  ```
			- side bar
			- side bar
			  collapsed:: true
				- tree.js
				- ```java
				  disabled: !wi(o)
				    
				    sName:"settings-select-popup",disabled:!wi(o),options:a})
				  改成disabled:false
				  ```
	- 奈菲竖向双语插件[[Netflix]]
	  id:: 6499b9b8-c9a1-4faf-a3b3-616b7d00a2b9
		- 正常安装，复制最新的连网js到本地js
		  logseq.order-list-type:: number
		  collapsed:: true
			- 39895-》禁止安装连网js
			  logseq.order-list-type:: number
				- logseq.order-list-type:: number
				  ```javascript
				  then((function(n) {
				                  e.log("哈哈哈哈");
				                  n.update = false;
				                  return n.update ? (e.log("Emergency update is enabled"),
				                  t(!0)) : (e.log("Emergency update is not enabled"),
				                  t(!1))
				              }
				  ```
			- 4185-》更改本地js
			  logseq.order-list-type:: number
				- logseq.order-list-type:: number
				  ```javascript
				                     value: function(e) {
				                          var t = {};
				                          e.accessLevel = "FULL";
				                          t.accessLevel = e.accessLevel,
				                          t.updatedTimeEpoch = Date.now(),
				                          window.localStorage.setItem(this.licenseStorageName, l(JSON.stringify(t)))
				                      }
				  ```
			- 8016
			  logseq.order-list-type:: number
				- logseq.order-list-type:: number
				  ```javascript
				                                , a = function() {
				                                  i.getLicense().then((function(e) {
				                                      var t = "No license";
				                                      e.accessLevel = "FULL";
				                                      t = "FULL" === e.accessLevel ? "Paid, You've been subscribing." : "FREE_TRIAL" === e.accessLevel ? "Free trial" : "yoyoyo",
				                                      document.getElementById("gLicenseStatus") && (document.getElementById("gLicenseStatus").textContent = t)
				                                  }
				  ```
		- 使用方法
		  logseq.order-list-type:: number
			- 关闭全局速度控制
			  logseq.order-list-type:: number
			- 【x】 打开每句暂停
			  logseq.order-list-type:: number
			- 【a】上一句
			  logseq.order-list-type:: number
			- 【d】下一句
			  logseq.order-list-type:: number
		-
	- 双语字幕
	  collapsed:: true
		- options.min.js
			- ```javascript
			  chrome.storage.sync.set({[e]:this.value})}function j(t){"OK"===t.response&&(d.innerText=chrome.i18n.getMessage("popup_active")||"Active"),"Not Found"===t.response&&(d.innerText=chrome.i18n.getMessage("popup_active")||"Active"),"Failed"===t.response&&(d.innerText=chrome.i18n.getMessage("popup_failed")||"Failed"),"internal"===t.response&&(d.innerText=(chrome.i18n.getMessage("popup_failed")||"Failed")+" (Internal)")}function T(t){return console.log("- Request: validate"),fetch("https://www.netflixsubs.app/validate?id="+t).then(t=>t.json()).then(t=>(chrome.storage.sync.set({statusUpdateAt:(new Date).getTime(),statusJson:JSON.stringify(t)}),t)).catch(t=>(console.log(t),{response:"internal"}))}function D(){chrome.storage.sync.get(["userId","statusUpdateAt","statusJson"],async({userId:t,statusUpdateAt:e,statusJson:n})=>{if(void 0!==t){let r=n?JSON.parse(n):void 0;
			  
			  ```
		- option.html - 275
			- ```htmlmixed
			          <span id="status" class="popup_active"></span>
			  
			  ```
-