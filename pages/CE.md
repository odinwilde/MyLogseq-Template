alias:: cheatengine

-
-
- 技巧
	- 开启DBVM，然后用目录下那个删除驱动
	-
-
- [[2023-11-06]] CE源码更改教程
	- 引用：
		- [1. 魔改CE 使用与演示 可过TP及大部分反作弊系统_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV19h4y1B7nF?p=1)
		- [魔改ce过eac总结 - tom lee - Medium](https://medium.com/@lctomlee/%E9%AD%94%E6%94%B9ce%E6%80%BB%E7%BB%93-1dc2c9f8ebc)
		- [数字证书伪造与利用（仅方便用于驱动开发人员的调试，不得非法使用） - 哔哩哔哩](https://www.bilibili.com/read/cv9802857/)
	- ---
	- 下载源码
		- [cheat-engine/cheat-engine: Cheat Engine. A development environment focused on modding](https://github.com/cheat-engine/cheat-engine)
	- 下载编译器
		- Download Lazarus 2.2.2 from
			- [https://sourceforge.net/projects/lazarus/files/Lazarus%20Windows%2064%20bits/Lazarus%202.2.2/](https://sourceforge.net/projects/lazarus/files/Lazarus%20Windows%2064%20bits/Lazarus%202.2.2/)
			- First install lazarus-2.2.2-fpc-3.2.2-win64.exe
			- and then lazarus-2.2.2-fpc-3.2.2-cross-i386-win32-win64.exe
		- 两个都要安装
	- 运行 Lazarus 并单击“项目”->“打开项目”。从 Cheat Engine 文件夹中选择 cheatengine.lpi 作为项目。
	- 依赖问题
		- 这里可能没有，右键有联网下载，可能需要 [[clash]] [[tun]]
	- UI界面与标题等信息
		- 改图标
		  collapsed:: true
			- ![image.png](../assets/image_1699267660990_0.png)
		- 改标题名称
		  collapsed:: true
			- Object Inspector 左边大列表
				- 点击最顶端的MainForm: TMainForm
				- 下面的properties-Caption
					- 更改名称即可
					- 名称字符长度要和Cheat Engine相同
		- 改其他地方名称
		  collapsed:: true
			- 顶栏-project-project Option
				- Application
					- 两处名称
					- 一处图标
				- Version info
				- Compiler Options
					- Paths
						- build modes
							- 只选择Release 64-bit O4 AVX2
								- o4是：
									- 更高级的指令
									- 附带32-bit
						- Target efile name
							- 更改名称
					-
	- 源代码内名称更改,进程和进程名，两处。
		- 顶栏-Project-unit
			- View Project Units
			- 搜main
				- 出
					- MainUnit.pas
					- MainUnit2.pas
						- ```java
						  const
						    ceversion=1.0.;
						    strVersionPart='1.0';
						  
						  
						  {$else}
						    strCheatEngine='xxxxx xxxxxxx';  
						  ```
	- 构建失败
	  collapsed:: true
		- RUN-clenup and build 清理后即可。
	- 构建
	  collapsed:: true
		- RUN-compile many modes-选择64bit-O4
		- 到cheat-engine-master\Cheat Engine\bin找对应文件即可。
	- ~~随机名称~~
	  collapsed:: true
		- MainUnit2.pas滑倒最下面
		- ```java
		  end;
		  
		  procedure initcetitle;
		  begin
		    CEnorm:=cename+BETA;  //.';       
		  ```
		- 添加更名函数
			- ```java
			  end;
			  
			  function GetRandomStr :string;
			  const
			       Chars ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk1mnopqrstuvwxyz0123456789!@#$%^&*0)_+-=[]{);\1:",.<>/?";
			  var
			     S: string;
			     i, N: integer;
			  begin
			       Randomize;
			       S := '';
			       for i := 1 to 10 do begin
			           N := Random(Length(Chars)) + 1;
			           S := S + Chars[N];
			      end;
			      result:=s;
			  end;
			  
			  procedure initcetitle;
			  begin
			    CEnorm:=cename+BETA;  //.';   
			  ```
	- 类名 暂时不管
	- 改文件内敏感字
		- [Downloads | mh-nexus](https://mh-nexus.de/en/downloads.php?product=HxD20)
			- 把exe拖进去
			- 搜索替换
				- 区分大小写-不勾选
				- 搜索方向-从头
				- 主要就是搜索 `Cheat Engine`/ ~~`cheat engine`~~/ `CheatEngine` / ~~`cheatengine`~~四个字符串，用项目编码和utf16编码，替换成自己自定义的名字，因为是编译好的exe，注意字符串长度要相同。
				- 更改unicode编码，再做相同操作
			- 文件-保存
			- https://mh-nexus.de/en/downloads.php?product=HxD20
		- 报错
			- `\cheat-engine-master\Cheat Engine\bin\autorun\emurpm.lua`
				- 33行
				- ```java
				  --find the emurpm.frm file
				  local ced=getCheatEngineDir()
				  local possiblepaths={}
				  ```
				- 更改getCheatEngineDir()到getxxxxxxxxxxxxDir()
	- 更改图标网址
	- 静态扫描
		- VMP3.5.0
		-