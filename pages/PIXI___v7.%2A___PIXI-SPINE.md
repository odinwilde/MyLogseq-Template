- [[spine/web]]
-
-
- v4
	- https://cdn.jsdelivr.net/npm/pixi-spine@4.*/dist/pixi-spine.js
	- https://www.npmjs.com/package/pixi-spine/v/4.0.4?activeTab=versions
-
- v4.1
	- 部署
	  id:: 65b68a4c-352d-4b52-8293-5828b6a7738f
		- [pixijs/spine at @pixi-spine/runtime-4.1_v3.1.2](https://github.com/pixijs/spine/tree/%40pixi-spine/runtime-4.1_v3.1.2)
		-
	- [脸上线条问题 Bug: Visual issue rendering spine on v7 · Issue #9141 · pixijs/pixijs](https://github.com/pixijs/pixijs/issues/9141#issuecomment-1573726217)
		- ```java
		    PIXI.Assets.setPreferences({
		      preferCreateImageBitmap: false,
		      preferWorker: false
		  });
		  ```
	-
-
-
- 热加载模型
	- ```javascript
	  let skelAss = modelName + "skel";
	  let atlasAss = modelName + "atlas";
	  //make sure add different keyname every time
	  PIXI.Assets.add(skelAss, "spine/"+modelName+"/"+modelName+".skel");
	  PIXI.Assets.add(atlasAss, "spine/"+modelName+"/"+modelName+".atlas");
	  
	  await PIXI.Assets.load([skelAss, atlasAss]);
	  ```
-