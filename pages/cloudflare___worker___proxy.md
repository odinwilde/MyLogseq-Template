-
	- https://github.com/3Kmfi6HP/EDtunnel/blob/main/_worker.js
	- 跳转 [利用Cloudflare进行网页跳转](https://blog.futrime.com/zh-cn/p/%E5%88%A9%E7%94%A8cloudflare%E8%BF%9B%E8%A1%8C%E7%BD%91%E9%A1%B5%E8%B7%B3%E8%BD%AC/)
	  collapsed:: true
		- ```js
		  async function handleRequest(request) {
		    return Response.redirect(someURLToRedirectTo, code)
		  }
		  addEventListener('fetch', async event => {
		    event.respondWith(handleRequest(event.request))
		  })
		  /**
		   * Example Input
		   * @param {Request} url where to redirect the response
		   * @param {number?=301|302} type permanent or temporary redirect
		   */
		  const someURLToRedirectTo = '<要跳转到的网址>'
		  const code = <跳转代码，可以是永久迁移(301)或者临时迁移(302)，建议301>
		  ```
		- 没啥用