-
	- [[WSL]] [[2023-09-12]]
		- 重置密码
			- [设置 WSL 开发环境 | Microsoft Learn](https://learn.microsoft.com/zh-cn/windows/wsl/setup/environment#set-up-your-linux-username-and-password)
			- [Windows10 WSL Ubuntu 忘记 root 密码如何重置 - 何大卫 - 博客园](https://www.cnblogs.com/heenhui2016/p/12916476.html)
	- [[WSL]] [[2023-07-18]]
		- 老版本迁移
			- https://community.anytype.io/t/anytype-legacy-to-beta-migration-trail-guide/9274
		- copy
			- ```shell
			  用到的命令发在这里方便复制
			  PowerShell
			  #设置WSL2为默认版本
			  wsl --set-default-version 2
			  
			  #查看当前安装的系统及版本
			  wsl -l -v
			  
			  #关闭WSL虚拟机
			  wsl --shutdown
			  
			  Ubuntu
			  #更新包信息
			  sudo apt update
			  
			  #安装xfce4
			  sudo apt install xfce4
			  
			  #编辑bashrc
			  nano ~/.bashrc
			  
			  #添加
			  export DISPLAY=:0
			  
			  #重新载入bashrc
			  source ~/.bashrc
			  
			  #运行xfce4
			  startxfce4
			  
			  #安装火狐
			  sudo apt install firefox
			  ```
		- 教程
			- [使用 WSL 运行 Linux GUI 应用 | Microsoft Learn](https://learn.microsoft.com/zh-cn/windows/wsl/tutorials/gui-apps)
			- [[限制wsl2占用过多内存 - 知乎](https://zhuanlan.zhihu.com/p/345645621)](https://zhuanlan.zhihu.com/p/345645621)
		- anytype fix
		  collapsed:: true
			- libgbm.so.1
				- [javascript - error while loading shared libraries: libgbm.so.1: cannot open shared object file: Puppeteer in Nodejs on AWS EC2 instance - Stack Overflow](https://stackoverflow.com/questions/67407104/error-while-loading-shared-libraries-libgbm-so-1-cannot-open-shared-object-fil)
					- ```
					  sudo apt-get update
					  sudo apt-get install -y libgbm-dev
					  ```
					- sudo find /usr/lib/ -name "libgbm*"
					  sudo ln -sf /usr/lib/x86_64-linux-gnu/libgbm.so.1 /usr/lib/libgbm.so.1
			- libgbm.so.2
				- [software installation - "libasound.so.2: cannot open shared object file: No such file or directory" - Ask Ubuntu](https://askubuntu.com/questions/482478/libasound-so-2-cannot-open-shared-object-file-no-such-file-or-directory)
					- ```
					  sudo apt-get install libasound2
					  ```
		- 跨文件访问
			- 直接windows用资源管理器
			-
-
-