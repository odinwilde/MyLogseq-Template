-
-
	- [[MOD]]制作 | [[Blender]]简单教程 2023-11-13]]
	  collapsed:: true
		- Mod制作关键工具
			- 游戏用插件
				- [Release V1.1 · SilentNightSound/SR-Model-Importer](https://github.com/SilentNightSound/SR-Model-Importer/releases/tag/1.1)
			- blender用插件
				- [blender_3dmigoto_srmi.py](https://github.com/SilentNightSound/SR-Model-Importer/releases/download/1.1/blender_3dmigoto_srmi.py) #blender_plugin
			- 游戏源模型
				- [SilentNightSound/SR-Model-Importer-Assets: Asset files that can be used with the SR-Model-Importer for a certain anime game number 2](https://github.com/SilentNightSound/SR-Model-Importer-Assets#sr-model-importer-assets)
			- 模型下载
				- [【崩坏：星穹铁道】卡芙卡](https://www.aplaybox.com/details/model/SsdCO8Gfu3Do)
		- blender 必备 插件
		  collapsed:: true
			- [blender_mmd_tools](https://github.com/UuuNyaa/blender_mmd_tools/releases/tag/v2.9.2) #blender_plugin
				- pmx格式
			- [一款旨在缩短时间的插件](https://github.com/absolute-quantum/cats-blender-plugin) #blender_plugin
				- 插件无法安装 [can't open cat · Issue #629 · absolute-quantum/cats-blender-plugin](https://github.com/absolute-quantum/cats-blender-plugin/issues/629)
					- The reason seems to be a ui change in version 3.6, which changed the original three categories of 'OFFICIAL' 'COMMUNITY' 'TESTING' to 'OFFICIAL' 'COMMUNITY'.
					- modification {'OFFICIAL', 'COMMUNITY', 'TESTING'} to {'OFFICIAL', 'COMMUNITY'}
			- [material-combiner-addon](https://github.com/Grim-es/material-combiner-addon) #blender_plugin
				- 材质组合
			- 其他
				- [汉化插件](https://blt.qa.pjcgart.com/questions/108)
		- [paint.net](https://www.dotpdn.com/downloads/pdn.html)
		  collapsed:: true
			- 贴图软件
			- [Alpha Mask Import Plugin (2.0) - Plugins - Publishing ONLY! - paint.net Forum](https://forums.getpaint.net/topic/1854-alpha-mask-import-plugin-20/)
				- Paint.net 翻转阿尔法插件
					- 5.0以下需要
					- 新版不用
			- python 下载地址（运行脚本）
				- https://www.python.org/downloads/
			- mod安装教程
				- https://b23.tv/sZ6GYyR
		- 前置简单设置
		  collapsed:: true
			- 中文
			  collapsed:: true
				- ![image.png](../assets/image_1699886445025_0.png){:height 443, :width 653}
			- 快捷键
				- ![image.png](../assets/image_1699886311243_0.png)
			- 安装插件
				- ![image.png](../assets/image_1699886945814_0.png)
				- 一个一个安装，不能全部安装。
				- 安装完再点一下。
			- 插件调试
			  collapsed:: true
				- matCombiner
					- 下载
						- ![image.png](../assets/image_1699887471828_0.png)
						- 重启
			- 模型导入
				- ![image.png](../assets/image_1699888267168_0.png){:height 812, :width 579}
			- 视图移动
			  collapsed:: true
				- 鼠标中键 转换视角
				- shitf+中 平移
				- ctrl+中+上下平移 放大缩小
			- 按Z 预览材质
			  collapsed:: true
				- ![image.png](../assets/image_1699888900025_0.png)
				- 按右键取消圆盘
			- 按N，选Cat，Fix Model
			- 按Tab，姿态模式，选中关节，按R转动，查看是否正常。
			- 导入游戏原始数据
			  collapsed:: true
				- ![image.png](../assets/image_1699889610540_0.png)
				- 全部选中导入
			- 调整比例
			  collapsed:: true
				- ![image.png](../assets/image_1699890028143_0.png)
					- 点中倒三角
					- 在视图中按S，即可调整
					- 调整和游戏内模型一样大小即可
			- 调整姿态
			  collapsed:: true
				- 点击骨骼-按Tab-选中姿态模式
				- 按R调整姿态，左键确认姿态
				- G 平移骨骼
			- mmd 反转
			- 应用骨骼
			  collapsed:: true
				- 物体模式，点击模型，body-数据-删除全部形态键
					- ![image.png](../assets/image_1699899780163_0.png)
					- ![image.png](../assets/image_1699899846109_0.png)
			- 改成父级
			  collapsed:: true
				- 物品模式-点击模型，右键
					- ![image.png](../assets/image_1699899930661_0.png)
				- 然后删除骨骼
			- 权重传递准备
			  collapsed:: true
				- 编辑模式，按3，Z打开透视，全选，按H隐藏，按1，出现小点。全部选中删除。
					- 按x/delelte 删除顶点
				- 删除全部权重顶点
				- 删除冗余部位
					- 选中原始模型，编辑模式，透视，
					- 按住L，多选
					- 按H隐藏
					- 按Alt+H恢复
				- 删除头部
					- 编辑模式，P，按材质分离，删除。
					- 右键合并模型。
			- 权重传递
			  collapsed:: true
				- ctrl 选中多个模型
					- ![image.png](../assets/image_1699902909655_0.png)
				- 权重绘制模式，顶栏第二行点击权重，左下角权重传递。
					- ![image.png](../assets/image_1699902855617_0.png)
				- 左下角，如下更改
					- ![image.png](../assets/image_1699902940096_0.png)
				- 数据-顶点组
					- 用脚本删除没用的权重
			- 贴图导出整合
			  collapsed:: true
				- 导出
					- ![image.png](../assets/image_1699903875771_0.png)
				- 导入
					- 顶栏-shading
					- ![image.png](../assets/image_1699903894276_0.png)
			- uv贴图改名
			  collapsed:: true
				- ![image.png](../assets/image_1699904054564_0.png)
				- 颜色属性
		- 删除原始模型，保留一点，S缩小
		- 合并模型到Body
			- 先选的会传递给后选的
		- 导出模型到orgin文件夹，会在上级目录创建Mod文件夹。
		- 修改贴图。
- 权重修改
	- F 改范围
	- 右边设置，相减
	- 编辑模式，选中或者L+shift选中，权重设置表，权重设置为0，然后指定。
	-
	-
-