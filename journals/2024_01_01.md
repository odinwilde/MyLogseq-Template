- [[git]] 多用户 upload 密钥
  collapsed:: true
	- 来源：https://www.zhihu.com/people/jiu-meng-40-42
	- 生成两个密钥
	  logseq.order-list-type:: number
	  collapsed:: true
		- `ssh-keygen -t rsa -C “yourEmail_1@gmail.com”`
		- `ssh-keygen -t rsa -C “yourEmail_2@gmail.com”`
		- > Generating public/private rsa key pair.
		  Enter file in which to save the key (------------------/.ssh/id_rsa):
			- 输入路径`F:\Users\username\.ssh\id_rsa`
			- 生成了一对秘钥id_rsa和id_rsa.pub ， 保存在了~/.ssh文件夹内。
	- 编辑配置文件
	  logseq.order-list-type:: number
	  collapsed:: true
		- 编辑~/.ssh/config文件。如果该文件不存在的话，直接创建一个就好。里面的内容如下：
		- ```java
		  # 公共
		  Host user_1
		  Hostname ssh.github.com
		  IdentityFile ~/.ssh/user_1
		  port 22
		  #个人
		  Host user_2
		  Hostname ssh.github.com
		  IdentityFile ~/.ssh/user_2
		  port 22
		  ```
		- 测试
			- 用git bash
				- `ssh -T git@user_1`
				- `ssh -T git@user_2`
			- 如果都能正常返回如下信息，就说明配置正常。
				- `Hi xxx! You've successfully authenticated, but GitHub does not provide shell access.`
	- 创建仓库，并更改所属用户。
	  logseq.order-list-type:: number
	- Clone时，指定用户
	  logseq.order-list-type:: number
	- 更改已经Clone下来的仓库到后设置的指定用户
	  logseq.order-list-type:: number
	  collapsed:: true
		- 使用以下命令查看当前远程仓库的 URL：
			- `git remote -v`
		- 然后，使用以下命令将远程仓库的 URL 修改为使用 SSH 协议：
			- `git remote set-url origin git@user_1:fishpigbird/live2d_1999_js.git`
		-
	- Github客户端问题
	  collapsed:: true
		- 无法管理多用户，只能是登录的git的用户的。
		-
	- 命令
	  collapsed:: true
		- local
			- 你可以使用以下命令来查看当前 Git 仓库的本地配置：
			- ```bash
			  git config --local --list
			  ```
			- 这将列出当前 Git 仓库的配置信息，包括用户名、邮箱等。确保在输出中可以看到正确的用户名和邮箱。
			- 如果你想要修改本地配置，可以使用类似的命令，例如：
			- ```bash
			  git config --local user.name "fish"
			  git config --local user.email "fishpigbird@outlook.com"
			  ```
			- 这样就会更新本地仓库的配置中的用户名和邮箱信息。请注意，这只会影响当前仓库，而不会影响全局配置或其他仓库的配置。
		- global
			- 你可以使用以下命令查看全局 Git 配置：
			- ```bash
			  git config --global --list
			  ```
			- 这会列出所有的全局配置，包括用户名、邮箱等信息。确保在输出中可以看到正确的用户名和邮箱。如果不正确，你可以使用以下命令修改它们：
			- ```bash
			  git config --global user.name "fish"
			  git config --global user.email "fishpigbird@outlook.com"
			  ```
			- 这样就会更新全局配置中的用户名和邮箱信息。然后，你可以再次尝试进行 Git 操作，确保新的配置生效。