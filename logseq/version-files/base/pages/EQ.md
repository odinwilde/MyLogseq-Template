-
-
	-
	- [Semaphore Already Exists](https://forums.steinberg.net/t/semaphore-already-exists/679872) [[EQ]] BUG [[2023-11-07]]
		- > You find the settings button on every VST plug in (on the Jbridge frame) which runs in Jbridge, when you open it inside Cubase. (Windows)
		   enable in jbridge settings :
		  **perfomance** mode
		  **audio Master Get Time hack**
		  **use thread safety measures in auxhost**
		  and on hardware plug ins also:
		  **run in existing auxhost**
		  this should solve your problem cheers
	- [[EQ]] 文件 #2023-10-25
		- GIthub
			- [jaakkopasanen/AutoEq: Automatic headphone equalization from frequency responses](https://github.com/jaakkopasanen/AutoEq)
		- 设备
			- Sony MDR-7506
				- [AutoEq/results/Rtings/over-ear/Sony MDR-7506 at master · jaakkopasanen/AutoEq](https://github.com/jaakkopasanen/AutoEq/tree/master/results/Rtings/over-ear/Sony%20MDR-7506)
				- [AutoEq/results/oratory1990/over-ear/Sony MDR-7506 at master · jaakkopasanen/AutoEq](https://github.com/jaakkopasanen/AutoEq/tree/master/results/oratory1990/over-ear/Sony%20MDR-7506)
				- [AutoEq/results/Headphone.com Legacy/over-ear/Sony MDR-7506 at master · jaakkopasanen/AutoEq](https://github.com/jaakkopasanen/AutoEq/tree/master/results/Headphone.com%20Legacy/over-ear/Sony%20MDR-7506)
				- nifo
					- ```markdown
					  
					  Sony MDR-7506 FixedBandEQ.txt
					  Sony MDR-7506 GraphicEQ.txt
					  Sony MDR-7506 ParametricEQ.txt
					  
					  
					  
					  
					  Sony MDR-7506 FixedBandEQ.txt：
					  这个文件可能包含了一组预设的均衡设置，其中每个频段的增益或削减都是固定的，无法进行自定义调整。这对于普通用户来说可能是最简单的选择，因为您只需选择一个预设，而无需自己进行频段调整。
					  
					  Sony MDR-7506 GraphicEQ.txt：
					  这个文件可能包含了一种图形均衡器（Graphic Equalizer）的设置，允许您自定义每个频段的增益或削减。通常，图形均衡器以可视方式呈现频段，您可以根据需要移动滑块或调整参数，以精确控制音频输出。这是一个更灵活的选项，适合那些希望自定义音频设置的用户。
					  
					  Sony MDR-7506 ParametricEQ.txt：
					  这个文件可能包含了一个参数均衡器（Parametric Equalizer）的设置，它提供更高级的音频调整功能。参数均衡器允许您调整中心频率、带宽和增益，以获得更精确的控制。这对于专业音频工程师或那些需要对音频进行详细微调的用户来说可能更有吸引力。
					  
					  您应该选择哪个文件取决于您的需求和技能水平：
					  
					  如果您只需要快速选择一种音频设置，您可以使用"FixedBandEQ.txt"中的固定设置。
					  如果您希望具有更多自定义控制并具有音频调整的经验，您可以使用"GraphicEQ.txt"，以可视方式自定义频段。
					  如果您是专业音频工程师或需要进行精确的音频微调，您可以使用"ParametricEQ.txt"，以更高级的方式自定义设置。
					  ```
					-
	- [[音乐]][[音质]] [[EQ]] [[2023-10-24]]
		- [xinleio/Headphones-simulate-stereo-speakers: 通过模拟立体声音箱串扰、模拟房间反射声以实现固定角度的模拟立体声音箱效果（优化耳机的空间印象）](https://github.com/xinleio/Headphones-simulate-stereo-speakers)
			- [【DSP试音】耳机模拟立体声音箱 辅助混响版_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1pP4y1z7TM/)
			- [混响插件以及桥接软件的用法_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1CG4y1F7Ut/)
				- jbridge一直识别不到混响那个32位dll，然后我网上下了一下微软运行库合集（不知道有没有关系），然后又用dx修复器把所有dll全部注册了，终于桥接成功了![[笑哭]](https://i0.hdslb.com/bfs/emote/c3043ba94babf824dea03ce500d0e73763bf4f40.png@48w_48h.webp)
		- ~~测试版本~~ 无法使用
			- 驱动：
				- 驱动程序提供商：Realtek Semiconductor Corp.
				- 驱动程序日期：29/5/2018
				- 驱动程序版本：6.0.1.8454
			- 安装模式：
				- no check ) origin
				- Install as SFX/EFX (experimental)
		- 驱动安装
			- 608703
				- [Realtek 瑞昱 声卡 Window10下无法使用Equalizer APO - 知乎](https://zhuanlan.zhihu.com/p/409446994)
					- msi B550-A PRO （网上的资料说声卡芯片是ALC892，
						- 经测试，安装6.0.9210.1声卡驱动无法使用Equalizer APO，安装6.0.1.8703可以使用。
				-
		- win11+笔记本内置显卡可用虚拟声卡+apo实现 [[2023-10-25]]
			- 安装Voicemeeter Banana
				- [Voicemeeter - 强大的虚拟声卡和混音器 | 米V米](https://www.mivm.cn/voicemeeter)
				  logseq.order-list-type:: number
					- [VB-Audio VoiceMeeter Banana](https://vb-audio.com/Voicemeeter/banana.htm)
					  logseq.order-list-type:: number
			- 安装1.3版本 Equalizer APO
				- [Equalizer APO - Browse Files at SourceForge.net](https://sourceforge.net/projects/equalizerapo/files/)
			- 在Equalizer APO中配置meeter中A1输出即可
		-
-
-
-